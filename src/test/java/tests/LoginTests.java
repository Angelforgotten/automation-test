package tests;

import static utils.extentreports.ExtentTestManager.startTest;

import data.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import java.lang.reflect.Method;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.listeners.TestListener;

@Listeners({TestListener.class})
@Epic("Regression Tests")
@Feature("Login Tests")
public class LoginTests extends BaseTest {
    @Test(priority = 0, description = "Valid Login Scenario with username and password.")
    public void LoginAuthLabor(Method method) {
        startTest(method.getName(), "Valid Login Scenario with valid username and password.");

        loginPage
                .goToLaborAuth()
                .loginBasicAuth(Constants.LoginAuth.username,Constants.LoginAuth.password)
                .loginLaborPage(Constants.LoginLabor.username,Constants.LoginLabor.companyCode,Constants.LoginLabor.password);

    }

}