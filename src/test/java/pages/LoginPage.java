package pages;

import data.Constants;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.logs.Log;

public class LoginPage extends BasePage {
    /**
     * Constructor
     */
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Web Elements
     */
    By usernameAuth = By.xpath("(//*[@id=\"signInFormUsername\"])[1]");
    By getPassWordAuth = By.xpath("(//*[@id=\"signInFormPassword\"])[1]");
    By idCompany = By.xpath("//input[@placeholder='企業ID']");
    By code = By.xpath("//input[@name='code_or_email']");
    By passWord = By.xpath("//input[@placeholder='パスワード']");
    By loginButton = By.xpath("//button[text()='ログイン']");
    By loginButtonAuth = By.xpath("(//*[@name=\"signInSubmitButton\"])[1]");

    /**
     * Page Methods
     */
    @Step("Opening Labor Website Login")
    public LoginPage goToLaborAuth() {
        Log.info("Opening Labor Website Login.");
        driver.get(Constants.BASE_URL_STG);
        return this;
    }

    @Step("Login auth Labor")
    public LoginPage loginBasicAuth(String username, String password) {
        Log.info("Trying to login the BaseAuth.");
        sendKey(usernameAuth, username);
        sendKey(getPassWordAuth, password);
        click(loginButtonAuth);
        return this;
    }

    @Step("Trying to login the Labor screen")
    public LoginPage loginLaborPage(String username, String codeCompany, String password) {
        Log.info("Trying to login the Labor screen.");
        sendKey(idCompany, username);
        sendKey(code, codeCompany);
        sendKey(passWord, password);
        click(loginButton);
        return this;
    }

}